using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Linq;
using System;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.XR;
using TMPro;

[System.Serializable]
public struct AuthOneResult
{
    [JsonProperty(PropertyName = "token_type")]
    public string TokenType { get; private set; }

    [JsonProperty(PropertyName = "expires_in")]
    public float ExpiresIn { get; private set; }

    [JsonProperty(PropertyName = "access_token")]
    public string AccessToken { get; private set; }

    [JsonProperty(PropertyName = "refresh_token")]
    public string RefreshToken { get; private set; }
}

[System.Serializable]
public struct AuthTwoResult
{
    [JsonProperty(PropertyName = "status")]
    public string TokenType { get; private set; }

    [JsonProperty(PropertyName = "message")]
    public float ExpiresIn { get; private set; }

    [JsonProperty(PropertyName = "data")]
    public string AccessToken { get; private set; }

    [JsonProperty(PropertyName = "refresh_token")]
    public string RefreshToken { get; private set; }
}

public class AuthTest : MonoBehaviour
{
    public AuthOneResult AuthOneResult { get; private set; }

    public TMP_InputField SecretText;
    public TextMeshProUGUI Auth1Text;

    public TextMeshProUGUI AuthCodeText;

    public TMP_InputField CustomId;
    public TextMeshProUGUI Guest_pp_id;

    public TextMeshProUGUI Google_pp_id;

    private PlayGamesClientConfiguration config;

    #region Google Sign In

    private void OnEnable()
    {
        config = new PlayGamesClientConfiguration.Builder()
            // // enables saving game progress.
            // .EnableSavedGames()
            // requests the email address of the player be available.
            // Will bring up a prompt for consent.
            .RequestEmail()
            .AddOauthScope("profile")
            // requests a server auth code be generated so it can be passed to an
            //  associated back end server application and exchanged for an OAuth token.
            .RequestServerAuthCode(false)
            // requests an ID token be generated.  This OAuth token can be used to
            //  identify the player to other services such as Firebase.
            // .RequestIdToken()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();


        //commented this with using a anonymous account and will link later
        //if somehow we can get the playerprefs that this user has signed in with google before, 
        // maybe try silent signin here

        //Assuming this is a silent signin 
        //
        // PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.NoPrompt, OnSignInResult);
    }

    public void SignInWithGoogle()
    {
        //This is called on the signup button on init and, link button in profile
        //callback route changes depending on if we have a currently logged in user
        Debug.Log("Sign in started");
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptAlways, OnSignInResult);
    }

    private void OnSignInResult(SignInStatus signInStatus)
    {
        if (signInStatus == SignInStatus.Success)
        {
            var serverAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();

            Debug.Log("Success auth code: " + serverAuthCode);
            StartCoroutine(GetToken2WithAuthCode(serverAuthCode));
            //TODO Sign in passion fab.
        }
        else
        {
            Debug.Log("*** Failed to authenticate with " + signInStatus);
        }
    }

    private IEnumerator GetToken2WithAuthCode(string authCode)
    {
        string url = "https://passionfab.herokuapp.com/api/v1/login";

        WWWForm form = new WWWForm();
        form.AddField("server_auth_token", authCode);
        form.AddField("hash", CreateHash(new string[] { authCode }));

        UnityWebRequest request = UnityWebRequest.Post(url, form);
        request.SetRequestHeader("Authorization", $"Bearer {AuthOneResult.AccessToken}");

        yield return request.SendWebRequest();

        Debug.Log(request.downloadHandler.text);
    }

    #endregion

    #region Google Auth Code

    public void GetAuthCode()
    {
        //This is called on the signup button on init and, link button in profile
        //callback route changes depending on if we have a currently logged in user
        Debug.Log("Sign in started");
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptAlways, AuthenticateResult);
    }

    private void AuthenticateResult(SignInStatus signInStatus)
    {
        if (signInStatus == SignInStatus.Success)
        {
            var serverAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            AuthCodeText.SetText(serverAuthCode);
            Debug.Log("Success auth code: " + serverAuthCode);
            //TODO Sign in passion fab.
        }
        else
        {
            Debug.Log("*** Failed to authenticate with " + signInStatus);
        }
    }
    #endregion

    #region Token One
    public void GetPassionFabTokenOne()
    {
        StartCoroutine(GetPassionFabTokenOneRoutine());
    }

    private IEnumerator GetPassionFabTokenOneRoutine()
    {
        string url = "https://passionfab.herokuapp.com/oauth/token";

        WWWForm form = new WWWForm();
        form.AddField("client_id", 2);
        form.AddField("client_secret", SecretText.text);
        form.AddField("grant_type", "password");
        form.AddField("username", "test@berenceylan.com");
        form.AddField("password", "youCanNeverCrack");
        form.AddField("scope", "code-auth fetch-data");

        UnityWebRequest request = UnityWebRequest.Post(url, form);

        yield return request.SendWebRequest();

        Debug.Log(request.downloadHandler.text);
        AuthOneResult = JsonConvert.DeserializeObject<AuthOneResult>(request.downloadHandler.text);
        Auth1Text.SetText(AuthOneResult.AccessToken);
    }

    #endregion

    #region Guest Sign In
    public void SignUpAsGuest()
    {
        StartCoroutine(SignUpAsGuestRoutine());
    }

    private IEnumerator SignUpAsGuestRoutine()
    {
        string url = "https://passionfab.herokuapp.com/api/v1/login";

        WWWForm form = new WWWForm();
        form.AddField("client_id", 2);
        form.AddField("custom_id", CustomId.text);
        form.AddField("hash", CreateHash(new string[] { SecretText.text, CustomId.text }));

        UnityWebRequest request = UnityWebRequest.Post(url, form);
        request.SetRequestHeader("Authorization", $"Bearer {AuthOneResult.AccessToken}");
        yield return request.SendWebRequest();

        Debug.Log(request.downloadHandler.text);
    }

    #endregion

    #region Hash

    private string CreateHash(string[] arr, int offset = 10)
    {
        string salt1 = RandomString(offset);
        //string salt1 = "enes";
        string salt2 = GetFirstTenCharactersOfArray(arr, offset);
        string key = string.Join(",", arr);
        string hash = ComputeSha256Hash(key + salt1 + salt2) + salt1;

        return hash;
    }

    private string GetFirstTenCharactersOfArray(string[] arr, int offset = 10)
    {
        string returnValue = "";

        for (int i = 0; i < arr.Length; i++)
        {
            returnValue += arr[i].Substring(0, offset);

            if (i != arr.Length - 1)
            {
                returnValue += ",";
            }
        }

        return returnValue;
    }

    private string ComputeSha256Hash(string rawData)
    {
        // Create a SHA256   
        using (SHA256 sha256Hash = SHA256.Create())
        {
            // ComputeHash - returns byte array  
            byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }

    private static System.Random random = new System.Random();
    public static string RandomString(int length)
    {
        const string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    #endregion
}

public static class StringExtension
{
    public static string GetLast(this string source, int tail_length)
    {
        if (tail_length >= source.Length)
            return source;
        return source.Substring(source.Length - tail_length);
    }
}